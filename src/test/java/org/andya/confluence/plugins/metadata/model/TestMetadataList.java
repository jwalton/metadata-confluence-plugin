/*
 * Copyright (c) 2006, 2007 Andy Armstrong, Kelsey Grant and other contributors.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice,
 *       this list of conditions and the following disclaimer in the documentation
 *       and/or other materials provided with the distribution.
 *     * The names of contributors may not
 *       be used to endorse or promote products derived from this software without
 *       specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.andya.confluence.plugins.metadata.model;

import com.atlassian.renderer.v2.macro.MacroException;

import java.util.List;

import org.andya.confluence.plugins.metadata.MetadataTestCase;
import org.junit.Test;

/**
 * Unit tests for the {@link MetadataList} model.
 */
public class TestMetadataList extends MetadataTestCase {
	private static final String COMPLEX_LINK = "[Atlassian|http://www.atlassian.com/]";
	private static final String COMPLEX_MACRO = "{complex-macro|param=y}[Atlassian|http://www.atlassian.com/]{complex-macro}";

	@Test
	public void testMetadataList() throws Exception {
		MetadataList list = new MetadataList();
		list.addRow("||Name|value|");
		list.addRow("|| Name   |   value   |");
		list.addRow("    ");  // blank rows should be ignored
		list.addRow("||Name| " + COMPLEX_LINK + " |");
		list.addRow("||Name|   " + COMPLEX_MACRO + "|");
		list.addRow("||Name||");
		list.addRow("||Name| |");
		List<String> names = list.getNames();
		List<String> values = list.getValues();
		assertEquals("Six names should be returned", 6, names.size());
		assertEquals("Six values should be returned", 6, values.size());
		for (int i=0; i < names.size(); i++) {
			assertEquals("Name " + i + " should be 'Name'", "Name", names.get(i));
		}
		assertEquals("Value 0 should be 'value'", "value", values.get(0));
		assertEquals("Value 1 should be 'value'", "value", values.get(1));
		assertEquals("Value 2 should be 'value'", COMPLEX_LINK, values.get(2));
		assertEquals("Value 3 should be 'value'", COMPLEX_MACRO, values.get(3));
		assertEquals("Value 4 should be ''", "", values.get(4));
		assertEquals("Value 5 should be ''", "", values.get(5));

		// Verify that more values don't throw exceptions
		list.addRow("||Status|{done}|");
		list.addRow("||Ship Date|11/29/2005|");
		list.addRow("||Notes|[official release announcement|dev:2.1 Release]|");

		// Now verify that some invalid rows throw a macro exception
		assertAddRowThrowsException("|");
		assertAddRowThrowsException("||");
		assertAddRowThrowsException("|||");
		assertAddRowThrowsException("|||value|");
		assertAddRowThrowsException("|| |value|");
	}

	private void assertAddRowThrowsException(String row) {
		MetadataList list = new MetadataList();
		try {
			list.addRow(row);
			fail("Invalid row should throw MacroException");
		} catch (MacroException e) {
			// ok
		}
	}
}
