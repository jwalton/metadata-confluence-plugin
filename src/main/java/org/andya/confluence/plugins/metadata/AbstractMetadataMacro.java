/*
 * Copyright (c) 2006, 2007 Andy Armstrong, Kelsey Grant and other contributors.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice,
 *       this list of conditions and the following disclaimer in the documentation
 *       and/or other materials provided with the distribution.
 *     * The names of contributors may not
 *       be used to endorse or promote products derived from this software without
 *       specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.andya.confluence.plugins.metadata;

import com.atlassian.confluence.core.ConfluenceEntityObject;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.ContentPropertyManager;
import com.atlassian.confluence.core.FormatSettingsManager;
import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.RenderContextOutputType;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.SubRenderer;
import com.atlassian.renderer.v2.macro.BaseMacro;
import com.atlassian.renderer.v2.macro.MacroException;
import com.atlassian.user.User;
import com.atlassian.user.UserManager;

import java.util.Map;
import java.util.List;
import java.util.Comparator;
import java.util.Collections;

import org.andya.confluence.utils.page.FindPage;
import org.andya.confluence.utils.MacroUtils;
import org.andya.confluence.utils.MacroConstants;
import org.andya.confluence.utils.ContentService;
import org.andya.confluence.plugins.metadata.model.MetadataContent;
import org.andya.confluence.plugins.metadata.model.MetadataList;
import org.andya.confluence.plugins.metadata.model.MetadataValue;
import org.andya.confluence.plugins.metadata.model.MetadataSortOrder;
import org.andya.confluence.plugins.metadata.renderers.MetadataRenderer;
import org.andya.confluence.plugins.metadata.renderers.MetadataListRenderer;
import org.andya.confluence.plugins.metadata.renderers.MetadataTableRenderer;

/**
 * The base class of all metadata macros.
 */
public abstract class AbstractMetadataMacro extends BaseMacro implements MacroConstants {

	private final String macroName;

	private SubRenderer subRenderer;
	private SpaceManager spaceManager;
	private PageManager pageManager;
	private UserManager userManager;
	private ContentService contentService;
	private ContentPropertyManager contentPropertyManager;
	private PermissionManager permissionManager;
	private LabelManager labelManager;
	private FormatSettingsManager formatSettingsManager;

	protected AbstractMetadataMacro(String macroName) {
		this.macroName = macroName;
	}

	public String getMacroName() {
		return macroName;
	}

	public SpaceManager getSpaceManager() {
		return spaceManager;
	}

	public void setSpaceManager(SpaceManager spaceManager) {
		this.spaceManager = spaceManager;
	}

	public PageManager getPageManager() {
		return pageManager;
	}

	public void setPageManager(PageManager pageManager) {
		this.pageManager = pageManager;
	}

	public UserManager getUserManager() {
		return userManager;
	}

	public void setUserManager(UserManager userManager) {
		this.userManager = userManager;
	}

	public FormatSettingsManager getFormatSettingsManager() {
		return formatSettingsManager;
	}

	public void setFormatSettingsManager(FormatSettingsManager formatSettingsManager) {
		this.formatSettingsManager = formatSettingsManager;
	}

	public PermissionManager getPermissionManager() {
		return permissionManager;
	}

	public void setPermissionManager(PermissionManager permissionManager) {
		this.permissionManager = permissionManager;
	}

	public ContentPropertyManager getContentPropertyManager() {
		return contentPropertyManager;
	}

	public void setContentPropertyManager(ContentPropertyManager contentPropertyManager) {
		this.contentPropertyManager = contentPropertyManager;
	}

	protected SubRenderer getSubRenderer() {
		return subRenderer;
	}

	public void setSubRenderer(SubRenderer subRenderer) {
		this.subRenderer = subRenderer;
	}

	public LabelManager getLabelManager() {
		return labelManager;
	}

	public void setLabelManager(LabelManager manager) {
		this.labelManager = manager;
	}

	/** Returns the current user. */
	public User getUser() {
		return AuthenticatedUserThreadLocal.getUser();
	}

	/** Returns the entity for the specified render context. */
	protected ContentEntityObject getEntity(RenderContext renderContext) {
		if (renderContext instanceof PageContext) {
			PageContext pageContext = (PageContext)renderContext;
			return pageContext.getEntity();
		}
		return null;
	}

	protected Page getPage(RenderContext renderContext) {
		ContentEntityObject ceo = getEntity(renderContext);
		if (ceo instanceof Page)
			return (Page)ceo;
		return null;
	}

	protected Page getPage(RenderContext renderContext, String pageName) throws MacroException {
		return getPage(renderContext, pageName, true);
	}

	protected Page getPage(RenderContext renderContext, String pageName, boolean required) throws MacroException {
		if (SELF_KEY.equalsIgnoreCase(pageName) || PARENT_KEY.equalsIgnoreCase(pageName)) {
			ContentEntityObject ceo = getEntity(renderContext);
			if (ceo instanceof Page) {
				Page page = (Page)ceo;
				if (PARENT_KEY.equalsIgnoreCase(pageName)) {
					page = page.getParent();
					if (page == null && required)
						throw new MacroException("No parent for page: " + page);
				}
				return page;
			}
			if (required)
				throw new MacroException("Cannot use " + pageName + " on a non-page: " + ceo);
			return null;
		}
		return new FindPage(getPageManager()).find(pageName, renderContext, null, required);
	}

	/**
	 * Returns a named metadata value from the specified content object. The value is returned
	 * as unrendered Wiki content so that it can be rendered appropriately on another page.
	 * @param ceo The content object to return the metadata for.
	 * @param valueName The name of the metadata value.
	 * @return The unrendered Wiki content for the metadata value, or null if none is found.
	 */
	public MetadataValue getMetadataValue(ContentEntityObject ceo, String valueName) {
		return getMetadataValue(ceo, valueName, null);
	}

	/**
	 * Returns a named metadata value from the specified content object. The value is returned
	 * as unrendered Wiki content so that it can be rendered appropriately on another page.
	 * @param ceo The content object to return the metadata for.
	 * @param valueName The name of the metadata value.
	 * @param defaultValue The default value to return if none is set.
	 * @return The unrendered Wiki content for the metadata value, or null if none is found.
	 */
	public MetadataValue getMetadataValue(ContentEntityObject ceo, String valueName, String defaultValue) {
		return MetadataUtils.getMetadataValue(getContentService(), ceo, valueName, defaultValue);
	}

	public ContentService getContentService() {
		ContentService contentService = this.contentService;
		if (contentService == null) {
			contentService = new ContentService(getContentPropertyManager(), getSpaceManager(), getUserManager(),
					getFormatSettingsManager());
			this.contentService = contentService;
		}
		return contentService;
	}

	/**
	 * Sets a named metadata value for the specified content object. The value specified should
	 * be unrendered Wiki content so that the user of the value can choose how to render it.
	 * @param ceo The content object to attach metadata to.
	 * @param valueName The name of the metadata value.
	 * @param unrenderedValue The unrendered Wiki content to be stored.
	 */
	public void setMetadataValue(ContentEntityObject ceo, String valueName, String unrenderedValue) {
		MetadataUtils.setMetadataValue(getContentPropertyManager(), ceo, valueName, unrenderedValue);
	}

	/**
	 * Sets a named metadata value for the content object associated with the current render context.
	 * The value specified should be unrendered Wiki content so that the user of the value can choose
	 * how to render it.
	 * @param renderContext The render context in which this macro is executing.
	 * @param valueName The name of the metadata value.
	 * @param unrenderedValue The unrendered Wiki content to be stored.
	 */
	public void setMetadataValue(RenderContext renderContext, String valueName, String unrenderedValue) {
		MetadataUtils.setMetadataValue(getContentPropertyManager(), renderContext, valueName, unrenderedValue);
	}

	/** Renders a page value for the given render context. */
	protected String renderValue(RenderContext renderContext, MetadataValue value) {
		String wikiSnippet = value != null ? value.getWikiSnippet() : "";
		return renderString(renderContext, wikiSnippet);
	}

	/** Renders a content entity object's metadata value. */
	protected String renderValue(RenderContext renderContext, ContentEntityObject ceo, MetadataValue value) {
		if (renderContext instanceof PageContext) {
			PageContext pageContext = new PageContext(ceo, (PageContext)renderContext);
			return renderValue(pageContext, value);
		} else {
			return renderValue(renderContext, value);
		}
	}

	/** Renders a string for the given render context. */
	protected String renderString(RenderContext renderContext, String string) {
		return getSubRenderer().render(string != null ? string : "", renderContext,
				renderContext.getRenderMode().and(RenderMode.suppress(RenderMode.F_FIRST_PARA)));
	}

	/** Renders a string for the specified content entity object. */
	protected String renderString(RenderContext renderContext, ContentEntityObject ceo, String string) {
		if (renderContext instanceof PageContext) {
			PageContext pageContext = new PageContext(ceo, (PageContext)renderContext);
			return renderString(pageContext, string);
		} else {
			return renderString(renderContext, string);
		}
	}

	/** Renders an exception to be displayed from a macro. */
	@SuppressWarnings("unchecked")
	protected String showRenderedExceptionString(Map parameters, Throwable e) {
		return MacroUtils.showRenderedExceptionString(parameters, "Unable to show \"" + getMacroName() + "\"", e);
	}

	/** Renders an exception to be displayed from a macro. */
	@SuppressWarnings("unchecked")
	protected String showRenderedExceptionString(Map parameters, String message) {
		return MacroUtils.showRenderedExceptionString(parameters, "Unable to show \"" + getMacroName() + "\"", message);
	}

	@SuppressWarnings("unchecked")
	protected String showUnrenderedExceptionString(Map parameters, Throwable e) {
		return MacroUtils.showUnrenderedExceptionString(parameters, "Unable to show \"" + getMacroName() + "\"", e);
	}

	@SuppressWarnings("unchecked")
	protected String showUnrenderedExceptionString(Map parameters, String message) {
		return MacroUtils.showUnrenderedExceptionString(parameters, "Unable to show \"" + getMacroName() + "\"", message);
	}

	protected String addMetadataList(Space space, String body, boolean hidden, boolean horizontal, String outputType)
			throws MacroException {
		return addMetadataList(space.getDescription(), body, hidden, horizontal, outputType);
	}

	protected String addMetadataList(ContentEntityObject ceo, String body, boolean hidden, boolean horizontal, String outputType)
			throws MacroException {
		MetadataList list = MetadataList.parseMetadataList(body);
		StringBuffer buffer = new StringBuffer();
		List<String> names = list.getNames();
		List<String> values = list.getValues();
		if(outputType == null || !outputType.equals(RenderContextOutputType.PREVIEW))
		{
			for (int i=0; i < names.size(); i++) {
				String valueName = (String)names.get(i);
				String value = (String)values.get(i);
				if(ceo != null)
					setMetadataValue(ceo, valueName, value);
			}
		}
		if (hidden)
			return "";
		if (horizontal) {
			for (int i=0; i < names.size(); i++) {
				String valueName = (String)names.get(i);
				buffer.append("||");
				buffer.append(valueName);
			}
			buffer.append("||\n");
			for (int i=0; i < names.size(); i++) {
				String value = (String)values.get(i);
				buffer.append("|");
				buffer.append(value.length() == 0 ? " " : value);
			}
			buffer.append("|\n");
		} else {
			for (int i=0; i < names.size(); i++) {
				String valueName = (String)names.get(i);
				String value = (String)values.get(i);
				buffer.append("||");
				buffer.append(valueName);
				buffer.append("|");
				buffer.append(value.length() == 0 ? " " : value);
				buffer.append("|\n");
			}
		}
		return buffer.toString();
	}

	/** Returns the preferred metadata renderer for the current macro. */
	@SuppressWarnings("unchecked")
	public MetadataRenderer getMetadataRenderer(Map parameters) throws MacroException {
		String style = MacroUtils.getStringParameter(parameters, STYLE_PARAMETER, DEFAULT_STYLE);
		if (LIST_STYLE_KEY.equals(style) || OL_STYLE_KEY.equals(style) || UL_STYLE_KEY.equals(style))
			return new MetadataListRenderer(getSubRenderer(), style);
		else if (TABLE_STYLE_KEY.equals(style))
			return new MetadataTableRenderer(getSubRenderer());
		else
			throw new MacroException("Unrecognized style " + style + " for metadata renderer");
	}

	@SuppressWarnings("unchecked")
	public List<MetadataContent> getSortedContents(Map parameters, List<ConfluenceEntityObject> entities, String defaultSortColumn) throws MacroException {
		List<MetadataContent> contents = MetadataUtils.getMetadataContent(getContentService(), entities);
		if (defaultSortColumn != null) {
			String commaDelimitedSort = MacroUtils.getStringParameter(parameters, SORT_PARAMETER, "");
			MetadataSortOrder[] sortOrders = MetadataSortOrder.getMetadataSortOrders(commaDelimitedSort);
			Comparator<MetadataContent> comparator = MetadataSortOrder.getComparator(sortOrders, defaultSortColumn);
			Collections.sort(contents, comparator);
		}
		return contents;
	}
}
