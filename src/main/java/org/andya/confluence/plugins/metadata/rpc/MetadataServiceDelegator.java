/*
 * Copyright (c) 2006, 2007 Andy Armstrong, Kelsey Grant and other contributors.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice,
 *       this list of conditions and the following disclaimer in the documentation
 *       and/or other materials provided with the distribution.
 *     * The names of contributors may not
 *       be used to endorse or promote products derived from this software without
 *       specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.andya.confluence.plugins.metadata.rpc;

import com.atlassian.confluence.rpc.RemoteException;
import com.atlassian.confluence.core.ContentPropertyManager;
import com.atlassian.confluence.core.FormatSettingsManager;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.user.UserManager;
import org.andya.confluence.plugins.metadata.MetadataUtils;
import org.andya.confluence.plugins.metadata.model.MetadataValue;
import org.andya.confluence.utils.ContentService;

/**
 * This is the delegator that does the real work for the metadata service.
 */
public class MetadataServiceDelegator implements MetadataService {
	private ContentPropertyManager contentPropertyManager;
	private SpaceManager spaceManager;
	private UserManager userManager;
	private PageManager pageManager;
	private FormatSettingsManager formatSettingsManager;

	public String login(String username, String password) throws RemoteException {
		throw new UnsupportedOperationException("Should be handled in an interceptor.");
	}

	public boolean logout(String token) {
		throw new UnsupportedOperationException("Should be handled in an interceptor.");
	}

	public MetadataValue[] getMetadataValues(String token, String spaceKey, String pageName) throws RemoteException {
		Page page = getPageManager().getPage(spaceKey, pageName);
		if (page == null)
			throw new RemoteException("Unable to find page \"" + pageName + "\" in space " + spaceKey);
		ContentService contentService = new ContentService(getContentPropertyManager(), getSpaceManager(), getUserManager(),
				getFormatSettingsManager());
		return MetadataUtils.getMetadataValues(contentService, page);
	}

	public ContentPropertyManager getContentPropertyManager() {
		return contentPropertyManager;
	}

	public void setContentPropertyManager(ContentPropertyManager contentPropertyManager) {
		this.contentPropertyManager = contentPropertyManager;
	}

	public SpaceManager getSpaceManager() {
		return spaceManager;
	}

	public void setSpaceManager(SpaceManager spaceManager) {
		this.spaceManager = spaceManager;
	}

	public UserManager getUserManager() {
		return userManager;
	}

	public void setUserManager(UserManager userManager) {
		this.userManager = userManager;
	}

	public PageManager getPageManager() {
		return pageManager;
	}

	public void setPageManager(PageManager pageManager) {
		this.pageManager = pageManager;
	}

	public FormatSettingsManager getFormatSettingsManager() {
		return formatSettingsManager;
	}

	public void setFormatSettingsManager(FormatSettingsManager formatSettingsManager) {
		this.formatSettingsManager = formatSettingsManager;
	}
}
