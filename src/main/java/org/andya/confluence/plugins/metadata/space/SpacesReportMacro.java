/*
 * Copyright (c) 2006, 2007 Andy Armstrong, Kelsey Grant and other contributors.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice,
 *       this list of conditions and the following disclaimer in the documentation
 *       and/or other materials provided with the distribution.
 *     * The names of contributors may not
 *       be used to endorse or promote products derived from this software without
 *       specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.andya.confluence.plugins.metadata.space;

import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceDescription;
import com.atlassian.confluence.core.ConfluenceEntityObject;
import com.atlassian.confluence.labels.ParsedLabelName;
import com.atlassian.confluence.labels.LabelParser;
import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.MacroException;

import org.andya.confluence.plugins.metadata.model.MetadataContent;
import org.andya.confluence.plugins.metadata.renderers.MetadataRenderer;
import org.andya.confluence.utils.MacroUtils;
import org.andya.confluence.utils.ContentService;

import java.util.*;

/**
 * Macro allowing metadata from a space to be shown on the current page.
 */
public class SpacesReportMacro extends AbstractSpaceMetadataMacro {
	public SpacesReportMacro() {
		super("space-report");
	}

	public boolean isInline() {
		return true;
	}

	public boolean hasBody() {
		return true;
	}

	public RenderMode getBodyRenderMode()
	{
		return RenderMode.NO_RENDER;
	}

	@SuppressWarnings("unchecked")
	public String execute(Map parameters, String body, RenderContext renderContext) throws MacroException {
		try {
			String commaDelimitedColumns = MacroUtils.getStringParameter(parameters, "0", true);
			int maxResults = MacroUtils.getIntParameter(parameters, MAX_RESULTS_PARAMETER, DEFAULT_MAX_RESULTS);
			List<MetadataContent> contents = getMatchingSpaces(parameters, renderContext, true);
			String[] columns = commaDelimitedColumns.split(",");
			String commaDelimitedTotals = MacroUtils.getStringParameter(parameters, TOTALS_PARAMETER, "");
			MetadataRenderer renderer = getMetadataRenderer(parameters);
			return renderer.render(renderContext, contents, maxResults, columns, commaDelimitedTotals, "Space");
		}
        catch (MacroException e) {
            return MacroUtils.showRenderedExceptionString(parameters, "Unable to render macro", e.getMessage());
        }
        catch (Exception e) {
			return MacroUtils.showRenderedExceptionString(parameters, "Unable to render macro", e);
		}
	}

	/**
	 * Returns a list of matching spaces based on the parameters to the macro.
	 * @param parameters The macro's parameters.
	 * @param renderContext The render context.
	 * @param ordered If true then the results should be ordered according to the sort parameters.
	 * @return A list of matching spaces
	 * @throws MacroException Thrown if any of the parameters are inappropriate.
	 */
	@SuppressWarnings("unchecked")
	public List<MetadataContent> getMatchingSpaces(Map parameters, RenderContext renderContext, boolean ordered) throws MacroException {
		String type = MacroUtils.getStringParameter(parameters, TYPE_PARAMETER, null);
		String labelName = MacroUtils.getStringParameter(parameters, LABEL_PARAMETER, null);
		boolean personalOnly = PERSONAL_SPACE_KEY.equalsIgnoreCase(type);
		SpaceManager spaceManager = getSpaceManager();
		List<Space> spaces;
		ParsedLabelName ref = labelName != null ? LabelParser.parse(labelName) : null;
		if (ref != null) {
			LabelManager labelManager = getLabelManager();
			Label label = labelManager.getLabel(ref);
			spaces = labelManager.getSpacesWithLabel(label);
		} else {
			spaces = spaceManager.getPermittedSpaces(getUser());
		}
		if (personalOnly) {
			List<Space> filteredSpaces = new ArrayList<Space>();
			for (Space space : spaces) {
				if (space.isPersonal())
					filteredSpaces.add(space);
			}
			spaces = filteredSpaces;
		}
		List<ConfluenceEntityObject> spaceDescriptions = new ArrayList<ConfluenceEntityObject>();
		for (Space space : spaces) {
			SpaceDescription spaceDescription = space.getDescription();
			spaceDescriptions.add(spaceDescription);
		}
		return getSortedContents(parameters, spaceDescriptions, ordered ? ContentService.SPACE_KEY : null);
	}
}
