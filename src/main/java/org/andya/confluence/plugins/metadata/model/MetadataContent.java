/*
 * Copyright (c) 2006, 2007 Andy Armstrong, Kelsey Grant and other contributors.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice,
 *       this list of conditions and the following disclaimer in the documentation
 *       and/or other materials provided with the distribution.
 *     * The names of contributors may not
 *       be used to endorse or promote products derived from this software without
 *       specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.andya.confluence.plugins.metadata.model;

import com.atlassian.confluence.core.ConfluenceEntityObject;

import java.util.Map;
import java.util.HashMap;
import java.util.Date;

import org.andya.confluence.utils.ContentService;
import org.andya.confluence.plugins.metadata.MetadataUtils;

/**
 * This represents all of the metadata stored for a content entity object.
 */
public class MetadataContent {
	private final ContentService contentService;
	private final ConfluenceEntityObject entity;
	private final Map<String, MetadataValue> values = new HashMap<String, MetadataValue>();

	public MetadataContent(ContentService contentService, ConfluenceEntityObject entity) {
		this.contentService = contentService;
		this.entity = entity;
	}

	public ContentService getContentService() {
		return contentService;
	}

	public ConfluenceEntityObject getEntity() {
		return entity;
	}

	public void putMetadataValue(MetadataValue value) {
		values.put(value.getName(), value);
	}

	/**
	 * Returns the value for the specified metadata name or null if none.
	 *
	 * @param name The name of the metadata.
	 * @return The metadata value or null if there is none.
	 */
	public MetadataValue getMetadataValue(String name) {
		return getMetadataValue(name, null);
	}

	/**
	 * Returns the value for the specified metadata name.
	 *
	 * @param name The name of the metadata.
	 * @param defaultValue The default value if there is no metadata value.
	 * @return The metadata value or the default value if there is none.
	 */
	public MetadataValue getMetadataValue(String name, Object defaultValue) {
		Map<String, MetadataValue> values = this.values;
		ContentService contentService = getContentService();
		if (values.containsKey(name)) {
			MetadataValue value = (MetadataValue)values.get(name);
			if (value != null)
				return value;
		} else {
			ConfluenceEntityObject entity = getEntity();
			if (contentService != null && entity != null) {
				MetadataValue value = MetadataUtils.getMetadataValue(contentService, entity, name, null);
				if (value != null) {
					putMetadataValue(value);
					return value;
				}
			}
		}
		return contentService.createMetadataValue(name, null, defaultValue);
	}

	public Number getMetadataNumber(String name) {
		return getMetadataNumber(name, null);
	}

	public Number getMetadataNumber(String name, Number defaultValue) {
		MetadataValue value = getMetadataValue(name);
		return value != null ? value.getValueAsNumber() : defaultValue;
	}

	public Date getMetadataDate(String name) {
		MetadataValue value = getMetadataValue(name);
		return value != null ? value.getValueAsDate() : null;
	}

	public String getMetadataWikiSnippet(String name) {
		return getMetadataWikiSnippet(name, "");
	}

	public String getMetadataWikiSnippet(String name, String defaultSnippet) {
		MetadataValue value = getMetadataValue(name);
		return value != null ? value.getWikiSnippet() : defaultSnippet;
	}

	public String toString() {
		return "MetadataContent{" +
				"ceo=" + getEntity() +
				"}";
	}
}
