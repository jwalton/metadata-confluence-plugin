/*
 * Copyright (c) 2006, 2007 Andy Armstrong, Kelsey Grant and other contributors.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice,
 *       this list of conditions and the following disclaimer in the documentation
 *       and/or other materials provided with the distribution.
 *     * The names of contributors may not
 *       be used to endorse or promote products derived from this software without
 *       specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.andya.confluence.plugins.metadata;

import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.macro.MacroException;
import com.atlassian.renderer.v2.RenderMode;

import java.util.Map;
import java.util.List;
import java.util.ArrayList;

import org.andya.confluence.utils.MacroUtils;
import org.andya.confluence.plugins.metadata.model.MetadataContent;
import org.andya.confluence.plugins.metadata.model.MetadataValue;

/**
 * This macro returns the total number of pages whose values for a particular piece of metadata
 * match its body. It is primarily useful for charting, e.g. how many pages match 'complete' vs
 * how many match 'in progress'.
 */
public class MetadataMatchesMacro extends MetadataUsingMacro {
	public MetadataMatchesMacro() {
		super("metadata-matches");
	}

	public boolean isInline() {
		return true;
	}

	public boolean hasBody() {
		return true;
	}

	public RenderMode getBodyRenderMode() {
		return null;
	}

	@SuppressWarnings("unchecked")
	public String execute(Map parameters, String body, RenderContext renderContext) throws MacroException	{
		try {
			String valueName = MacroUtils.getStringParameter(parameters, "0", true);
			String functionName = MacroUtils.getStringParameter(parameters, FUNCTION_PARAMETER, DEFAULT_FUNCTION);
			String calculationValueName = MacroUtils.getStringParameter(parameters, VALUE_NAME_PARAMETER, null);
			List<MetadataContent> contents = getMatchingPages(parameters, getContentService(), renderContext, false);
			List<Number> values = new ArrayList<Number>();
			int total = 0;
			for (MetadataContent content : contents) {
				MetadataValue value = content.getMetadataValue(valueName);
				String wikiSnippet = value != null ? value.getWikiSnippet() : "";
				if (body.equals(wikiSnippet)) {
					if (calculationValueName != null) {
						MetadataValue calculationValue = content.getMetadataValue(calculationValueName);
						Number number = calculationValue.getValueAsNumber();
						if (number != null)
							values.add(number);
					} else {
						total++;
					}
				}
			}
			if (values.isEmpty()) {
				return Integer.toString(total);
			} else {
				return renderCalculation(functionName, values);
			}
		} catch (Exception e) {
			return showUnrenderedExceptionString(parameters, e);
		}
	}
}
