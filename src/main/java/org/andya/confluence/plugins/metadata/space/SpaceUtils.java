/*
 * Copyright (c) 2006, 2007 Andy Armstrong, Kelsey Grant and other contributors.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice,
 *       this list of conditions and the following disclaimer in the documentation
 *       and/or other materials provided with the distribution.
 *     * The names of contributors may not
 *       be used to endorse or promote products derived from this software without
 *       specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.andya.confluence.plugins.metadata.space;

import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceDescription;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.core.ContentPropertyManager;
import com.atlassian.user.User;
import com.atlassian.user.UserManager;
import com.atlassian.user.EntityException;
import org.andya.confluence.plugins.metadata.MetadataUtils;
import org.andya.confluence.plugins.metadata.model.MetadataValue;
import org.andya.confluence.utils.ContentService;

/**
 * A set of utilities for useful metadata-based management of spaces.
 */
public class SpaceUtils {
	public static final String PARENT_SPACE_METADATA_NAME = "Parent";
	public static final String USERS_SPACE_METADATA_NAME = "UsersSpace";

	/** Returns the owner of the personal space, or null if none. */
	public static User getPersonalSpaceOwner(UserManager userManager, Space space) {
		if (!space.isPersonal())
			return null;
		// todo: is there ever a case where the creator is not the user themselves?
		String userName = space.getCreatorName();
		try {
			return userManager.getUser(userName);
		} catch (EntityException e) {
			return null;
		}
	}

	/** Returns the parent space of the specified space. */
	public static Space getParentSpace(ContentService contentService, Space space) {
		SpaceDescription description = space.getDescription();
		MetadataValue parentKey = MetadataUtils.getRawMetadataValue(contentService, description, PARENT_SPACE_METADATA_NAME, null);
		SpaceManager spaceManager = contentService.getSpaceManager();
		return parentKey != null ? spaceManager.getSpace(parentKey.getWikiSnippet()) : null;
	}

	/** Sets the parent space of the specified space. */
	public static void setParentSpace(ContentService contentService, Space space, Space parentSpace) {
		SpaceDescription spaceDescription = space.getDescription();
		ContentPropertyManager contentPropertyManager = contentService.getContentPropertyManager();
		String parentKey = parentSpace.getKey();
		MetadataUtils.setMetadataValue(contentPropertyManager, spaceDescription, PARENT_SPACE_METADATA_NAME, parentKey);
	}

	/** Returns true if the specified space is the root of all user spaces. */
	public static boolean isUsersSpace(ContentService contentService, Space space) {
		SpaceDescription description = space.getDescription();
		MetadataValue isUsersSpaceValue = MetadataUtils.getRawMetadataValue(contentService, description, USERS_SPACE_METADATA_NAME, null);
		Boolean isUsersSpace = isUsersSpaceValue != null ? isUsersSpaceValue.getValueAsBoolean() : null;
		return isUsersSpace != null && isUsersSpace.booleanValue();
	}

	/** Sets whether the specified space is the root of all user spaces. */
	public static void setUsersSpace(ContentService contentService, Space space, boolean isUsersSpace) {
		SpaceDescription spaceDescription = space.getDescription();
		ContentPropertyManager contentPropertyManager = contentService.getContentPropertyManager();
		MetadataUtils.setMetadataValue(contentPropertyManager, spaceDescription, USERS_SPACE_METADATA_NAME,
				isUsersSpace ? "true" : "false");
	}
}
