/*
 * Copyright (c) 2006, 2007 Andy Armstrong, Kelsey Grant and other contributors.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice,
 *       this list of conditions and the following disclaimer in the documentation
 *       and/or other materials provided with the distribution.
 *     * The names of contributors may not
 *       be used to endorse or promote products derived from this software without
 *       specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.andya.confluence.plugins.metadata.space;

import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.macro.MacroException;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.confluence.spaces.SpaceDescription;

import java.util.Map;

import org.andya.confluence.utils.MacroUtils;
import org.andya.confluence.plugins.metadata.model.MetadataValue;

/**
 * Macro allowing metadata from a space to be shown on the current page.
 */
public class SpaceMetadataFromMacro extends AbstractSpaceMetadataMacro {
	public SpaceMetadataFromMacro() {
		super("space-metadata-from");
	}

	public boolean isInline() {
		return true;
	}

	public boolean hasBody() {
		return true;
	}

	public RenderMode getBodyRenderMode() {
		return RenderMode.NO_RENDER;
	}

	@SuppressWarnings("unchecked")
	public String execute(Map parameters, String qualifiedName, RenderContext renderContext) throws MacroException {
		try {
			String spaceKey = MacroUtils.getStringParameter(parameters, "0", null);
			String valueName = MacroUtils.getStringParameter(parameters, "1", null);

			if (spaceKey == null && valueName == null) {
				throw new MacroException("Must specify the space and the metadata to fetch");
			}

			if (valueName == null) {
				//shift along..
				valueName = spaceKey;
				spaceKey = SELF_KEY;
			}

			if ("".equals(spaceKey)) {
				spaceKey = SELF_KEY;
			}
			SpaceDescription spaceDescription;
			if (spaceKey != null && !SELF_KEY.equals(spaceKey))
				spaceDescription = getSpaceDescription(spaceKey);
			else
				spaceDescription = getSpaceDescription(renderContext);
			MetadataValue value = getMetadataValue(spaceDescription, valueName);
			return renderValue(renderContext, value);
		} catch (Exception e) {
			return showRenderedExceptionString(parameters, e);
		}
	}
}
