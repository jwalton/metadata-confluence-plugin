/*
 * Copyright (c) 2006, 2007 Andy Armstrong, Kelsey Grant and other contributors.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice,
 *       this list of conditions and the following disclaimer in the documentation
 *       and/or other materials provided with the distribution.
 *     * The names of contributors may not
 *       be used to endorse or promote products derived from this software without
 *       specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.andya.confluence.plugins.metadata;

import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.MacroException;
import com.atlassian.renderer.RenderContext;
import com.atlassian.confluence.pages.Page;

import java.util.Map;

import org.andya.confluence.utils.MacroUtils;
import org.andya.confluence.utils.ContentService;
import org.andya.confluence.plugins.metadata.model.MetadataValue;

/**
 * This macro lists all of the metadata values from another page.
 */
public class MetadataValuesMacro extends MetadataUsingMacro {
	public MetadataValuesMacro() {
		super("metadata-values");
	}

	public boolean isInline() {
		return false;
	}

	public RenderMode getBodyRenderMode() {
		return RenderMode.NO_RENDER;
	}

	@SuppressWarnings("unchecked")
	public String execute(Map parameters, String body, RenderContext renderContext) throws MacroException {
		try {
			String pageName = MacroUtils.getStringParameter(parameters, "0", true);
			boolean horizontal = MacroUtils.getBooleanParameter(parameters, "orientation", "horizontal", false);
			Page page = getPage(renderContext, pageName);
			ContentService contentService = getContentService();
			String[] names = MetadataUtils.getMetadataNames(contentService, page);
			if (names == null || names.length == 0)
				return renderString(renderContext, "{warn}No metadata is attached to page \"" + pageName + "\"{warn}\n");
			StringBuffer buffer = new StringBuffer();
			if (horizontal) {
				for (int i=0; i < names.length; i++) {
					String name = names[i];
					buffer.append("||");
					buffer.append(name);
				}
				buffer.append("||\n");
				for (int i=0; i < names.length; i++) {
					String name = names[i];
					MetadataValue value = getMetadataValue(page, name, "");
					String valueString = value != null ? value.getWikiSnippet() : "";
					buffer.append("|");
					buffer.append(valueString.length() == 0 ? " " : valueString);
				}
				buffer.append("|\n");
			} else {
				for (int i=0; i < names.length; i++) {
					String name = names[i];
					MetadataValue value = getMetadataValue(page, name, "");
					String valueString = value != null ? value.getWikiSnippet() : "";
					buffer.append("||");
					buffer.append(name);
					buffer.append("|");
					buffer.append(valueString.length() == 0 ? " " : valueString);
					buffer.append("|\n");
				}
			}
			return renderString(renderContext, page, buffer.toString());
		} catch (Exception e) {
			return showRenderedExceptionString(parameters, e);
		}
	}
}
