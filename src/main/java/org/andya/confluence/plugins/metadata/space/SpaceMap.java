/*
 * Copyright (c) 2006, 2007 Andy Armstrong, Kelsey Grant and other contributors.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice,
 *       this list of conditions and the following disclaimer in the documentation
 *       and/or other materials provided with the distribution.
 *     * The names of contributors may not
 *       be used to endorse or promote products derived from this software without
 *       specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.andya.confluence.plugins.metadata.space;

import com.atlassian.user.User;
import com.atlassian.confluence.spaces.Space;

import java.util.Map;
import java.util.HashMap;
import java.util.List;

import org.andya.confluence.utils.ContentService;

/**
 * This class represents all of the information about the spaces in Confluence.
 */
public class SpaceMap {
	private final SpaceHierarchy rootHierarchy;
	private SpaceHierarchy usersHierarchy;
	private final Map<Space, SpaceHierarchy> map = new HashMap<Space, SpaceHierarchy>();

	private SpaceMap(User user, ContentService contentService) {
		rootHierarchy = new SpaceHierarchy(null, null);
		populate(user, contentService);
	}

	public static SpaceMap get(User user, ContentService contentService) {
		// todo: is there a safe way to cache this given the possibility of running in a cluster?
		return new SpaceMap(user, contentService);
	}

	public SpaceHierarchy getRootHierarchy() {
		return rootHierarchy;
	}

	public SpaceHierarchy getUsersHierarchy() {
		return usersHierarchy;
	}

	private Map<Space, SpaceHierarchy> getMap() {
		return map;
	}

	public SpaceHierarchy getSpaceHierarchy(Space space) {
		return (SpaceHierarchy)getMap().get(space);
	}

	public void populate(User user, ContentService contentService) {
		Map<Space, SpaceHierarchy> map = getMap();
		List<Space> spaces = contentService.getSpaceManager().getPermittedSpaces(user);

		// First add the regular spaces
		for (Space space : spaces) {
			if (space.getKey() != null && getSpaceHierarchy(space) == null && !space.isPersonal())
				addSpaceHierarchy(map, contentService, space);
		}

		// Now add the personal spaces
		for (Space space : spaces) {
			if (space.getKey() != null && getSpaceHierarchy(space) == null && space.isPersonal())
				addSpaceHierarchy(map, contentService, space);
		}
	}

	private SpaceHierarchy addSpaceHierarchy(Map<Space, SpaceHierarchy> spaceMap, ContentService contentService, Space space) {
		Space parent = SpaceUtils.getParentSpace(contentService, space);
		if (parent == null && space.isPersonal()) {
			SpaceHierarchy usersHierarchy = getUsersHierarchy();
			if (usersHierarchy != null)
				parent = usersHierarchy.getSpace();
		}
		SpaceHierarchy parentHierarchy = getRootHierarchy();
		if (parent != null) {
			parentHierarchy = (SpaceHierarchy)spaceMap.get(parent);
			if (parentHierarchy == null)
				parentHierarchy = addSpaceHierarchy(spaceMap, contentService, parent);
		}
		SpaceHierarchy spaceHierarchy = (SpaceHierarchy)spaceMap.get(space);
		if (spaceHierarchy == null) {
			spaceHierarchy = new SpaceHierarchy(space, parentHierarchy);
			spaceMap.put(space, spaceHierarchy);
		}
		boolean isUsersSpace = SpaceUtils.isUsersSpace(contentService, space);
		if (isUsersSpace)
			usersHierarchy = spaceHierarchy;
		return spaceHierarchy;
	}
}
