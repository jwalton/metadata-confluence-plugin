/*
 * Copyright (c) 2006, 2007 Andy Armstrong, Kelsey Grant and other contributors.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice,
 *       this list of conditions and the following disclaimer in the documentation
 *       and/or other materials provided with the distribution.
 *     * The names of contributors may not
 *       be used to endorse or promote products derived from this software without
 *       specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.andya.confluence.plugins.metadata.rpc;

import com.atlassian.confluence.rpc.RemoteException;
import com.atlassian.confluence.rpc.xmlrpc.Translator;

import java.util.Vector;

/**
 * The implementation of the {@link MetadataXmlRpcHandler}.
 */
public class MetadataXmlRpcHandlerImpl implements MetadataXmlRpcHandler {
	private MetadataService metadataServiceDelegator;

	public String login(String username, String password) throws RemoteException {
		return metadataServiceDelegator.login(username, password);
	}

	public boolean logout(String token) throws RemoteException {
		return metadataServiceDelegator.logout(token);
	}

	@SuppressWarnings("unchecked")
	public Vector getMetadataValues(String token, String spaceKey, String pageName) throws RemoteException {
		return Translator.makeVector(metadataServiceDelegator.getMetadataValues(token, spaceKey, pageName));
	}

	// Accessor methods
	public void setMetadataServiceDelegator(MetadataService metadataServiceDelegator) {
		this.metadataServiceDelegator = metadataServiceDelegator;
	}
}
