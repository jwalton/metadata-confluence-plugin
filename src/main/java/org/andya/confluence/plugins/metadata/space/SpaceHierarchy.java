/*
 * Copyright (c) 2006, 2007 Andy Armstrong, Kelsey Grant and other contributors.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice,
 *       this list of conditions and the following disclaimer in the documentation
 *       and/or other materials provided with the distribution.
 *     * The names of contributors may not
 *       be used to endorse or promote products derived from this software without
 *       specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.andya.confluence.plugins.metadata.space;

import com.atlassian.confluence.spaces.Space;
import com.atlassian.user.User;

import java.util.*;

import org.andya.confluence.utils.ContentService;

/**
 * A simple class that represents a space hierarchy.
 */
public class SpaceHierarchy {
	private final Space space;
	private final SpaceHierarchy parent;
	private final List<SpaceHierarchy> children = new ArrayList<SpaceHierarchy>();

	public SpaceHierarchy(Space space, SpaceHierarchy parent) {
		this.space = space;
		this.parent = parent;
		if (parent != null)
			parent.addChildSpace(this);
	}

	public Space getSpace() {
		return space;
	}

	public SpaceHierarchy getParent() {
		return parent;
	}

	public List<SpaceHierarchy> getChildren() {
		return children;
	}

	public void addChildSpace(SpaceHierarchy space) {
		children.add(space);
	}

	public static SpaceHierarchy getSpaceHierarchy(User user, ContentService contentService) {
		List<Space> spaces = contentService.getSpaceManager().getPermittedSpaces(user);
		Map<Space, SpaceHierarchy> spaceMap = new HashMap<Space, SpaceHierarchy>();
		SpaceHierarchy rootHierarchy = new SpaceHierarchy(null, null);
		spaceMap.put(null, rootHierarchy);
		for (Space space : spaces) {
			createSpaceHierarchy(spaceMap, contentService, space);
		}
		return rootHierarchy;
	}

	private static SpaceHierarchy createSpaceHierarchy(Map<Space, SpaceHierarchy> spaceMap, ContentService contentService, Space space) {
		Space parent = SpaceUtils.getParentSpace(contentService, space);
		SpaceHierarchy parentHierarchy = null;
		if (parent != null) {
			parentHierarchy = (SpaceHierarchy)spaceMap.get(parent);
			if (parentHierarchy == null)
				parentHierarchy = createSpaceHierarchy(spaceMap, contentService, space);
		}
		SpaceHierarchy spaceHierarchy = (SpaceHierarchy)spaceMap.get(space);
		if (spaceHierarchy == null) {
			spaceHierarchy = new SpaceHierarchy(space, parentHierarchy);
			spaceMap.put(space, spaceHierarchy);
		}
		return spaceHierarchy;
	}
}
