/*
 * Copyright (c) 2006, 2007 Andy Armstrong, Kelsey Grant and other contributors.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice,
 *       this list of conditions and the following disclaimer in the documentation
 *       and/or other materials provided with the distribution.
 *     * The names of contributors may not
 *       be used to endorse or promote products derived from this software without
 *       specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.andya.confluence.plugins.metadata.model;

import com.atlassian.renderer.v2.macro.MacroException;

import java.util.Comparator;
import java.util.List;
import java.util.ArrayList;

import org.andya.confluence.utils.ContentService;
import org.andya.confluence.utils.MacroConstants;
import org.apache.commons.collections.comparators.ComparatorChain;

/**
 * Represents the sort order for a single metadata value.
 */
public class MetadataSortOrder {
	private String metadataName;
	private boolean ascending;
	protected String sortMethod;

	public MetadataSortOrder(String metadataName, boolean ascending, String sortMethod) {
		this.metadataName = metadataName;
		this.ascending = ascending;
		this.sortMethod = sortMethod;
	}

	public String getMetadataName() {
		return metadataName;
	}

	public boolean isAscending() {
		return ascending;
	}

	public Comparator<MetadataContent> getComparator() {
		return new Comparator<MetadataContent>() {
			public int compare(MetadataContent o1, MetadataContent o2) {
				String metadataName = getMetadataName();
				MetadataContent ceo1 = (MetadataContent)o1;
				MetadataContent ceo2 = (MetadataContent)o2;
				MetadataValue value1 = ceo1.getMetadataValue(metadataName);
				MetadataValue value2 = ceo2.getMetadataValue(metadataName);
				if (value1 == null)
					return value2 == null ? 0 : 1;
				else if (value2 == null)
					return -1;
				if(sortMethod.equals(MacroConstants.SORT_ISTRING))
					return (isAscending() ? 1 : -1) * value1.compareValueAsIStringTo(value2);
				if(sortMethod.equals(MacroConstants.SORT_STRING))
					return (isAscending() ? 1 : -1) * value1.compareValueAsStringTo(value2);
				if(sortMethod.equals(MacroConstants.SORT_NUMBER))
					return (isAscending() ? 1 : -1) * value1.compareValueAsNumberTo(value2);
				if(sortMethod.equals(MacroConstants.SORT_DATE))
					return (isAscending() ? 1 : -1) * value1.compareTo(value2);

				// Default parsing and comparison
				int result = value1.compareTo(value2);
				return isAscending() ? result : -result;
			}
		};
	}

	/**
	 * Returns a comparator based on an array of MetadataSortOrders.
	 * @param sortOrders An array of sort orders in order of priority
	 * @param defaultColumn the default column to sort by, if all else fails
	 * @return A comparator that can sort a collection of ContentEntityObjects
	 */
	@SuppressWarnings("unchecked")
	public static Comparator<MetadataContent> getComparator(MetadataSortOrder[] sortOrders, String defaultColumn) {
		List<Comparator<MetadataContent>> comparators = new ArrayList<Comparator<MetadataContent>>();
		if (sortOrders != null) {
			for (int i=0; i < sortOrders.length; i++) {
				MetadataSortOrder sortOrder = sortOrders[i];
				comparators.add(sortOrder.getComparator());
			}
		}
		// If all else fails then sort by title
		if (defaultColumn != null)
			comparators.add(new MetadataSortOrder(defaultColumn, true, MacroConstants.SORT_DEFAULT).getComparator());
		return new ComparatorChain(comparators);
	}

	/**
	 * Turns a comma-separated list of sort orders into an array of {@link MetadataSortOrder}s.
	 * e.g. the string "Date desc, Title" will return two sort orders
	 *
	 * @param commaSeparatedSorts
	 * @return
	 * @throws MacroException
	 */
	public static MetadataSortOrder[] getMetadataSortOrders(String commaSeparatedSorts) throws MacroException {
		commaSeparatedSorts = commaSeparatedSorts.trim();
		if (commaSeparatedSorts == null || commaSeparatedSorts.length() == 0)
			return null;
		String[] sortOrderStrings = commaSeparatedSorts.split(",");
		int sortOrderCount = sortOrderStrings.length;
		MetadataSortOrder[] sortOrders = new MetadataSortOrder[sortOrderCount];
		for (int i=0; i < sortOrderCount; i++) {
			String sortOrder = sortOrderStrings[i].trim();
			String sortMethod = MacroConstants.SORT_DEFAULT;
			if (sortOrder.length() == 0)
				throw new MacroException("Missing metadata name for sort value \"" + commaSeparatedSorts + "\"");

			if(sortOrder.contains(MacroConstants.SORT_DATE))
				sortMethod = MacroConstants.SORT_DATE;
			else if(sortOrder.contains(MacroConstants.SORT_NUMBER))
				sortMethod = MacroConstants.SORT_NUMBER;
			else if (sortOrder.contains(MacroConstants.SORT_STRING))
				sortMethod = MacroConstants.SORT_STRING;
			else if (sortOrder.contains(MacroConstants.SORT_ISTRING))
				sortMethod = MacroConstants.SORT_ISTRING;
			sortOrder = sortOrder.replace(sortMethod, "");

			int lastSpaceIndex = sortOrder.lastIndexOf(' ');
			String metadataName = sortOrder;

			boolean ascending = true;
			if (lastSpaceIndex >= 0) {
				String lastWord = sortOrder.substring(lastSpaceIndex + 1).toLowerCase();
				ascending = !lastWord.startsWith("desc");
				if (lastWord.startsWith("asc") || lastWord.startsWith("desc"))
					metadataName = sortOrder.substring(0, lastSpaceIndex);
			}
			sortOrders[i] = new MetadataSortOrder(metadataName, ascending, sortMethod);
		}
		return sortOrders;
	}
}
