/*
 * Copyright (c) 2006, 2007 Andy Armstrong, Kelsey Grant and other contributors.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice,
 *       this list of conditions and the following disclaimer in the documentation
 *       and/or other materials provided with the distribution.
 *     * The names of contributors may not
 *       be used to endorse or promote products derived from this software without
 *       specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.andya.confluence.plugins.metadata.space;

import org.andya.confluence.plugins.metadata.AbstractMetadataMacro;
import com.atlassian.confluence.spaces.SpaceDescription;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.pages.Page;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.macro.MacroException;

/**
 * The base class of all metadata-based space macros.
 */
public abstract class AbstractSpaceMetadataMacro extends AbstractMetadataMacro {
	public AbstractSpaceMetadataMacro(String macroName) {
		super(macroName);
	}

	protected Space getSpace(RenderContext renderContext) throws MacroException {
		if (renderContext instanceof PageContext) {
			PageContext pageContext = (PageContext)renderContext;
			String spaceKey = pageContext.getSpaceKey();
			return getSpaceManager().getSpace(spaceKey);
		}
		return null;
	}

	protected SpaceDescription getSpaceDescription(RenderContext renderContext) throws MacroException {
		if (renderContext instanceof PageContext) {
			PageContext pageContext = (PageContext)renderContext;
			String spaceKey = pageContext.getSpaceKey();
			return getSpaceDescription(spaceKey);
		}
		return null;
	}

	protected SpaceDescription getSpaceDescription(Page page) throws MacroException {
		return getSpaceDescription(page.getSpaceKey());
	}

	protected SpaceDescription getSpaceDescription(String spaceKey) throws MacroException {
		Space space = getSpaceManager().getSpace(spaceKey);
		return space.getDescription();
	}
}
