package org.andya.confluence.plugins.metadata;

import com.atlassian.confluence.core.ContentPropertyManager;
import com.atlassian.confluence.event.events.content.page.PageEvent;
import com.atlassian.confluence.event.events.content.page.PageRemoveEvent;
import com.atlassian.confluence.event.events.content.page.PageRestoreEvent;
import com.atlassian.confluence.event.events.content.page.PageTrashedEvent;
import com.atlassian.confluence.event.events.content.page.PageUpdateEvent;
import com.atlassian.event.Event;
import com.atlassian.event.EventListener;
import com.atlassian.renderer.WikiStyleRenderer;
import com.atlassian.spring.container.ContainerManager;

public class PageUpdateListener implements EventListener {

	private ContentPropertyManager contentPropertyManager;

	@SuppressWarnings("unchecked")
	private static final Class[] HANDLED_EVENTS = new Class[] {
			PageRestoreEvent.class, PageRemoveEvent.class,
			PageTrashedEvent.class, PageUpdateEvent.class };

	@SuppressWarnings("unchecked")
	public Class[] getHandledEventClasses() {
		return HANDLED_EVENTS;
	}

	public void handleEvent(Event event) {
		PageEvent pageEvent = (PageEvent) event;
		if (contentPropertyManager == null)
			contentPropertyManager = (ContentPropertyManager) ContainerManager
					.getComponent("contentPropertyManager");
		MetadataUtils.clearMetadata(contentPropertyManager, pageEvent
				.getContent());
		// If the page is restored or updated, force rendering to process
		// metadata
		if (pageEvent instanceof PageRestoreEvent
				|| pageEvent instanceof PageUpdateEvent) {
			WikiStyleRenderer renderer = (WikiStyleRenderer) ContainerManager
					.getComponent("wikiStyleRenderer");
			renderer.convertWikiToXHtml(pageEvent.getContent().toPageContext(),
					pageEvent.getPage().getContent());
		}

	}

}
