package org.andya.confluence.plugins.metadata;

import org.andya.confluence.plugins.metadata.model.MetadataValue;
import org.andya.confluence.utils.ContentService;
import org.randombits.confluence.supplier.SimpleSupplier;
import org.randombits.confluence.supplier.SupplierException;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.ContentPropertyManager;
import com.atlassian.confluence.core.FormatSettingsManager;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.spring.container.ContainerManager;
import com.atlassian.user.UserManager;

public class MetadataSupplier extends SimpleSupplier<ContentEntityObject> {

	private ContentService contentService;

	private ContentPropertyManager contentPropertyManager;

	private FormatSettingsManager formatSettingsManager;

	private SpaceManager spaceManager;

	private UserManager userManager;
	
	public MetadataSupplier() {
		super(ContentEntityObject.class, true, "metadata");
	}

	private ContentService getContentService() {
		ContentService contentService = this.contentService;
		if (contentService == null) {
			contentService = new ContentService(getContentPropertyManager(),
					getSpaceManager(), getUserManager(),
					getFormatSettingsManager());
			this.contentService = contentService;
		}
		return contentService;
	}

	private ContentPropertyManager getContentPropertyManager() {
		if (contentPropertyManager == null)
			contentPropertyManager = (ContentPropertyManager) ContainerManager
					.getComponent("contentPropertyManager");
		return contentPropertyManager;
	}

	private FormatSettingsManager getFormatSettingsManager() {
		if (formatSettingsManager == null)
			formatSettingsManager = (FormatSettingsManager) ContainerManager
					.getComponent("formatSettingsManager");
		return formatSettingsManager;
	}

	private SpaceManager getSpaceManager() {
		if (spaceManager == null)
			spaceManager = (SpaceManager) ContainerManager
					.getComponent("spaceManager");
		return spaceManager;
	}

	private UserManager getUserManager() {
		if (userManager == null)
			userManager = (UserManager) ContainerManager
					.getComponent("userManager");
		return userManager;
	}

	@Override
	protected Object findValue(ContentEntityObject entity, String key) throws SupplierException {
		if(key == null || "".equals(key))
			return MetadataUtils.getAllMetadata(getContentService(), entity).entrySet();
		MetadataValue v = MetadataUtils.getMetadataValue(getContentService(), entity, key,
				null);
		return v != null ? v.getWikiSnippet() : null;
	}

}
