package org.andya.confluence.plugins.metadata;

import org.randombits.confluence.intercom.ConnectionBundle;
import org.randombits.confluence.intercom.IntercomException;
import org.randombits.confluence.intercom.IntercomStartup;
import org.randombits.confluence.intercom.LocalIntercomListener;
import org.randombits.confluence.supplier.LocalSupplierBundle;

public class MetadataSupplierStartup extends IntercomStartup {

	public MetadataSupplierStartup() throws IntercomException {
		
	}
	@Override
	protected ConnectionBundle createConnectionBundle() {
		return new LocalSupplierBundle(new MetadataSupplier());
	}

	@Override
	protected LocalIntercomListener createLocalIntercomListener() {
		return null;
	}

}
