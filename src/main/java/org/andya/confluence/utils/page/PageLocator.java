/*
 * Copyright (c) 2006, 2007 Andy Armstrong, Kelsey Grant and other contributors.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice,
 *       this list of conditions and the following disclaimer in the documentation
 *       and/or other materials provided with the distribution.
 *     * The names of contributors may not
 *       be used to endorse or promote products derived from this software without
 *       specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.andya.confluence.utils.page;

import java.util.List;

import com.atlassian.confluence.pages.Page;
import com.atlassian.renderer.v2.macro.MacroException;

/**
 * Decides which page sources need to be used. Different combinations apply - see
 * http://confluence.atlassian.com/display/DISC/Unified+Page+Searching+Tags for more detail. 
 *
 * @author kelsey
 *
 */
public class PageLocator implements PageSource {
	public List<Page> getPages(PageSearchContext context) throws MacroException {

		// compatibility..
		if(context.hasParameter(LABEL_PARAMETER)) {
			context.setParameter(LABELS_PARAMETER, context.getStringParameter(LABEL_PARAMETER, true));
		}

		final PageSource total;
		//this is a nasty matrix of possibilities. Bear with me, and review
		//the wiki link above, as it makes each combination a little clearer.

		if(!context.hasParameter(ROOT_PARAMETER)) {
			context.setParameter(ROOT_PARAMETER, SELF_KEY); // not used in case 2, mind.
			//case 1:
			if(!context.hasParameter(PAGES_PARAMETER) && !context.hasParameter(LABELS_PARAMETER)) {
				context.setParameter(PAGES_PARAMETER, CHILDREN_KEY);
				total = new PagesPageSource(new RootPageSource());
			}//case 2
			else if(!context.hasParameter(PAGES_PARAMETER) && context.hasParameter(LABELS_PARAMETER)) {
				total = new LabelPageSource();
			}//case 3
			else if(context.hasParameter(PAGES_PARAMETER) && !context.hasParameter(LABELS_PARAMETER)) {
				total = new PagesPageSource(new RootPageSource());
			}//case 4
			else { // pages != null & labels != null...
				total = new LabelFilterPageSource(new PagesPageSource(new RootPageSource()));
			}
		} else {
			//case 5
			if(!context.hasParameter(PAGES_PARAMETER) && !context.hasParameter(LABELS_PARAMETER)) {
				context.setParameter(PAGES_PARAMETER, IDENTITY_KEY);
				total = new PagesPageSource(new RootPageSource());
			} //case 6
			else if(!context.hasParameter(PAGES_PARAMETER) && context.hasParameter(LABELS_PARAMETER)) {
				context.setParameter(PAGES_PARAMETER, IDENTITY_KEY);
				total = new LabelFilterPageSource(new PagesPageSource(new RootPageSource()));
			} //case 7
			else if(context.hasParameter(PAGES_PARAMETER) && !context.hasParameter(LABELS_PARAMETER)) {
				total = new PagesPageSource(new RootPageSource());
			} //case 8
			else {
				total = new LabelFilterPageSource(new PagesPageSource(new RootPageSource()));
			}
		}
		return total.getPages(context);
	}
}
