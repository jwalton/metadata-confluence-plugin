/*
 * Copyright (c) 2006, 2007 Andy Armstrong, Kelsey Grant and other contributors.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice,
 *       this list of conditions and the following disclaimer in the documentation
 *       and/or other materials provided with the distribution.
 *     * The names of contributors may not
 *       be used to endorse or promote products derived from this software without
 *       specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.andya.confluence.utils.page;

import com.atlassian.renderer.v2.macro.MacroException;
import org.andya.confluence.utils.parse.Expression;
import org.andya.confluence.utils.parse.ExpressionParser;

public abstract class AbstractLabelPageSource implements PageSource {

	private final ExpressionParser parser = new ExpressionParser();


	/**
	 * Get the expression for this label
	 * 
	 * @param context
	 * @return
	 * @throws MacroException
	 */
	protected Expression getExpression(PageSearchContext context) throws MacroException {
		String labelQuery = context.getStringParameter(LABELS_PARAMETER, true);

		String match = context.getStringParameter(MATCH_LABELS_PARAMETER, "" );
		// if they say to match by 'all' then we replace the OR operator with AND operator.
		if (match.toLowerCase().indexOf("all") != -1) {
			labelQuery = labelQuery.replace(',', '+');
		}

		final Expression labelExpression = parser.parse(labelQuery);
		return labelExpression;
	}

}
