/*
 * Copyright (c) 2006, 2007 Andy Armstrong, Kelsey Grant and other contributors.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice,
 *       this list of conditions and the following disclaimer in the documentation
 *       and/or other materials provided with the distribution.
 *     * The names of contributors may not
 *       be used to endorse or promote products derived from this software without
 *       specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.andya.confluence.utils.page.source;

import com.atlassian.confluence.pages.Page;
import com.atlassian.renderer.v2.macro.MacroException;

import java.util.LinkedList;
import java.util.List;

import org.andya.confluence.utils.page.PageSearchContext;

/**
 * This operates on each page in the child page source to produce a second set of pages
 * as a result. The viable parameters are @children, @ancestors, @descendants, and also
 * the special @identity - which really just tells it to pass through. Children cannot
 * be the default because if you specify a root, you may just want these pages, not their
 * children However if you specify neither, then it is the current page's children by default.
 * That case is handled by the PageLocator however.
 * 
 * @author kelsey
 *
 */
public class ScopePageSource implements PageSource {

	private final PageSource child;

	public ScopePageSource(PageSource child) {
		this.child = child;
	}

	public List<Page> getPages(PageSearchContext context) throws MacroException {
		final List<Page> entities = new LinkedList<Page>();
		String pagesCode = context.getStringParameter(SCOPE_PARAMETER, true);
		pagesCode = "@" + pagesCode.replaceAll("-", ""); //handled earlier

		final List<Page> rootPages = child.getPages(context);
		for (Page page : rootPages) {
			if(CHILDREN_KEY.equalsIgnoreCase(pagesCode)) {
				entities.addAll(page.getChildren());
			} else if (ANCESTORS_KEY.equalsIgnoreCase(pagesCode)) {
				entities.addAll(page.getAncestors());
			} else if (DESCENDENTS_KEY.equalsIgnoreCase(pagesCode) ||
					DESCENDANTS_KEY.equalsIgnoreCase(pagesCode)) {
				entities.addAll(page.getDescendents());
			} else {
				throw new MacroException("Unknown pages code '" + pagesCode + "'");
			}
		}

		return entities;
	}
}
