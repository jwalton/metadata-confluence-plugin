/*
 * Copyright (c) 2006, 2007 Andy Armstrong, Kelsey Grant and other contributors.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice,
 *       this list of conditions and the following disclaimer in the documentation
 *       and/or other materials provided with the distribution.
 *     * The names of contributors may not
 *       be used to endorse or promote products derived from this software without
 *       specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.andya.confluence.utils.page;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.macro.MacroException;
import org.andya.confluence.utils.MacroConstants;

/**
 * Locates pages. Supports @self, @parent, raw page names with an optional space key.
 * 
 * @author kelsey
 *
 */
public class FindPage {

	private final PageManager pageManager;

	public FindPage(PageManager pageManager) {
		this.pageManager = pageManager;
	}

	public Page find(String pageName, RenderContext renderContext, String spaceKey) throws MacroException {
		return find(pageName, renderContext, spaceKey, true);
	}

	public Page find(String pageName, RenderContext renderContext, String spaceKey, boolean required) throws MacroException {
		Page thisPage = null;
		if (renderContext instanceof PageContext) {
			ContentEntityObject ceo = ((PageContext)renderContext).getEntity();
			if (ceo instanceof Page) {
				thisPage = (Page)ceo;
				if (spaceKey == null || "".equals(spaceKey)) {
					// use our current space
					spaceKey = thisPage.getSpaceKey();
				}
			}
		}
		if (pageName.startsWith("@")) {
			if(thisPage == null) {
				if (required)
					throw new MacroException("Cannot get self/parent pages from context " + renderContext);
			} else {
				if (MacroConstants.SELF_KEY.equalsIgnoreCase(pageName)) {
					return thisPage;
				} else if (MacroConstants.PARENT_KEY.equalsIgnoreCase(pageName)) {
					return thisPage.getParent();
				} else if (required) {
					throw new MacroException("Page Name '" + pageName + "' is not valid");
				}
			}
			return null;
		} else {
			return getPage(renderContext, pageName, spaceKey, required);
		}
	}

	protected Page getPage(RenderContext renderContext, String pageName, String spaceKey, boolean required)
			throws MacroException {
		String[] pageSections = pageName.split(":");
		if (pageSections.length == 2) {
			return getPage(pageSections[0], pageSections[1], required);
		} else if (renderContext instanceof PageContext) {
			return getPage(spaceKey, pageName, required);
		}
		if (required)
			throw new MacroException("No such page \"" + pageName + "\"");
		return null;
	}

	protected Page getPage(String spaceKey, String pageName, boolean required) throws MacroException {
		Page page = pageManager.getPage(spaceKey, pageName);
		if (page == null && required)
			throw new MacroException("No such page \"" + pageName + "\"");
		return page;
	}

}
