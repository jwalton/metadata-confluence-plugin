/*
 * Copyright (c) 2006, 2007 Andy Armstrong, Kelsey Grant and other contributors.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice,
 *       this list of conditions and the following disclaimer in the documentation
 *       and/or other materials provided with the distribution.
 *     * The names of contributors may not
 *       be used to endorse or promote products derived from this software without
 *       specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.andya.confluence.utils.page.source;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;

import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.pages.Page;
import com.atlassian.renderer.v2.macro.MacroException;
import com.opensymphony.util.TextUtils;
import org.andya.confluence.utils.page.PageSearchContext;

public class ContentTypeFilterPageSource implements PageSource {

	private static final List<String> VALID_TYPES = Arrays.asList(new String[] {
			Page.CONTENT_TYPE, BlogPost.CONTENT_TYPE
		});

	private final PageSource child;
	private final String type;

	public ContentTypeFilterPageSource(PageSource child, String type) {
		this.child = child;
		this.type = type;
	}

	public List<Page> getPages(PageSearchContext context) throws MacroException {
		return filterByContentType(child.getPages(context), type);
	}

	private List<Page> filterByContentType(List<Page> entities, String type) throws MacroException {
		List<String> contentTypes = new ArrayList<String>();
		if (TextUtils.stringSet(type))
		{
			StringTokenizer st = new StringTokenizer(type, ",");
			while (st.hasMoreTokens()) {
				String contentType = st.nextToken();

				// Only add the content type if it's supported
				if (VALID_TYPES.contains(contentType))
					contentTypes.add(contentType);
				else
					throw new MacroException(contentType + " is not a supported content type.");
			}
		}

		// Only filter the content when we have had a type specified
		if (contentTypes.size() > 0)
			return filterByContentType(entities, contentTypes);
		return entities;
	}

	/**
	 * @param original
	 * @param types list of types (the ones you get from calling <ContentEntityObject>.getType())
	 * @return list of ContentEntityObjects whose type matches those specified
	 */
	private List<Page> filterByContentType(List<Page> original, List<String> types)	{
		List<Page> result = new ArrayList<Page>();
		for (Page page : original)
		{
			if (types.contains(page.getType()))
				result.add(page);
		}
		return result;
	}


}
